package com.example.jenya1.trackme;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.jenya1.trackme.service.LaunchService;
import com.example.jenya1.trackme.service.TrackerService;
import com.karan.churi.PermissionManager.PermissionManager;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TrackerActivity extends AppCompatActivity {
    private static final int PERMISSIONS_REQUEST = 1;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    PermissionManager permissionManager;


    String email,password;

    Button startjob,finishjob;
    //TextView timer;

    long MillisecondTime, StartTime, TimeBuff, UpdateTime = 0L ;


    int Hours,Seconds, Minutes, MilliSeconds ;

    Chronometer focus;
    Double latitude,longitude;

    int seconds;
    boolean running;

    String URL_CHECKIN = "http://etrack.rayvatapps.com/api/api.php?request=startjob";

    String URL_CHECKOUT="http://etrack.rayvatapps.com/api/api.php?request=endjob";

    String time;

    LocationManager manager;
    boolean statusOfGPS ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracker);

        getpermissions();

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.back);
        ab.setTitle("Work");
        ab.setDisplayHomeAsUpEnabled(true);

        manager=  (LocationManager) getSystemService(Context.LOCATION_SERVICE );

        //timer=(TextView)findViewById(R.id.tvtimer);
        startjob=(Button)findViewById(R.id.btnstartjob);
        finishjob=(Button)findViewById(R.id.btnstopjob);

        focus = (Chronometer) findViewById(R.id.chronometer1);

        Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/HKNova-Medium.ttf");
        startjob.setTypeface(tf);
        finishjob.setTypeface(tf);
        focus.setTypeface(tf);


        startjob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkLocationonoroff();
                if(statusOfGPS==true)
                {
                    if(TrackerService.tracker_service_status==0) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        time=sdf.format(new Date());
                        Log.e("Time",time);
                        starttracking();
                        TrackerService.tracker_service_status=1;
                    }
                    else
                    {
                        Toast.makeText(TrackerActivity.this, "You already started job", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Intent i=new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(i);
                    Toast.makeText(TrackerActivity.this, "Please Enable Location...", Toast.LENGTH_SHORT).show();
                }


                //startLaunchService();
                //starttracking();
            }
        });

        finishjob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkLocationonoroff();
                if(statusOfGPS==true)
                {
                    if(TrackerService.tracker_service_status==1 && LaunchService.launch_service_status==0 ) {
                        Double ar[] = new Double[2];
                        ar=TrackerService.getLocation();
                        latitude=ar[0];
                        longitude=ar[1];
                        Log.e("Job End","lat "+latitude+"long "+longitude);
                        chackout();
                        TrackerService.tracker_service_status=0;
                        stopService(new Intent(getApplicationContext(), TrackerService.class));
                        focus.stop();
                    }
                    else
                    {
                        if(LaunchService.launch_service_status==0)
                        {
                            Toast.makeText(TrackerActivity.this, "Start your job first", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(TrackerActivity.this, "Finish your launch break first", Toast.LENGTH_SHORT).show();
                        }

                    }
                }
                else
                {
                    Intent i=new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(i);

                    Toast.makeText(TrackerActivity.this, "Please Enable Location...", Toast.LENGTH_SHORT).show();
                }



            }
        });
        startcountdown();
    }

    private void checkLocationonoroff() {

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE );
        statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

       /* if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) )
            Toast.makeText(getApplicationContext(), "GPS is disable!", Toast.LENGTH_LONG).show();
        else
            Toast.makeText(getApplicationContext(), "GPS is Enable!", Toast.LENGTH_LONG).show();

        Log.e("location", String.valueOf(manager));*/

    }

    private void chackout() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_CHECKOUT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
                        try {
                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status = jobj.getString("status");

                            String msg = jobj.getString("message");

                            if (status.equals("1")) {
                                //move to next page
                                Toast.makeText(TrackerActivity.this, msg, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(TrackerActivity.this, msg, Toast.LENGTH_SHORT).show();
                            }

                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                        }
                        // Showing response message coming from server.
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.

                        if( volleyError instanceof NoConnectionError) {
                            Toast.makeText(TrackerActivity.this, "No connection available", Toast.LENGTH_SHORT).show();
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            Toast.makeText(TrackerActivity.this, "Oops. Timeout error!", Toast.LENGTH_LONG).show();
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            Toast.makeText(TrackerActivity.this, "Server error!", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(TrackerActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                email = sharedPreferences.getString("username","");
                password = sharedPreferences.getString("password","");

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                time=sdf.format(new Date());
                Log.e("Time",time);

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                //params.put("name", name);
                params.put("email", email);
                params.put("endtime", time);
                params.put("latitude", String.valueOf(latitude));
                params.put("longitude", String.valueOf(longitude));

                return params;
            }

        };
        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(TrackerActivity.this);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    private void checkin() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_CHECKIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
                        try {
                            JSONObject jobj = new JSONObject(ServerResponse);


                            String status = jobj.getString("status");

                            String msg = jobj.getString("message");

                            if (status.equals("1")) {
                                //move to next page
                                Toast.makeText(TrackerActivity.this, msg, Toast.LENGTH_SHORT).show();

                            } else {
                                Toast.makeText(TrackerActivity.this, msg, Toast.LENGTH_SHORT).show();
                            }

                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.

                        if( volleyError instanceof NoConnectionError) {
                            Toast.makeText(TrackerActivity.this, "No connection available", Toast.LENGTH_SHORT).show();
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            Toast.makeText(TrackerActivity.this, "Oops. Timeout error!", Toast.LENGTH_LONG).show();
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            Toast.makeText(TrackerActivity.this, "Server error!", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(TrackerActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                  email = sharedPreferences.getString("username","");
                  password = sharedPreferences.getString("password","");

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                //params.put("name", name);
                params.put("email", email);
                params.put("starttime", time);
                params.put("latitude", String.valueOf(latitude));
                params.put("longitude", String.valueOf(longitude));

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(TrackerActivity.this);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    private void starttracking() {
        // Check GPS is enabled
        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Toast.makeText(this, "Please enable location services", Toast.LENGTH_SHORT).show();
            finish();
        }

        // Check location permission is granted - if it is, start
        // the service, otherwise request the permission
        int permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permission == PackageManager.PERMISSION_GRANTED) {
            startTrackerService();
            startcountdown();

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 100ms
                    Double ar[] = new Double[2];
                    ar=TrackerService.getLocation();
                    latitude=ar[0];
                    longitude=ar[1];
                    Log.e("Job Start","lat "+latitude+"long "+longitude);

                    checkin();

                    //Toast.makeText(TrackerActivity.this, "toast", Toast.LENGTH_SHORT).show();
                }
            }, 10000);

            focus.start();
            /*StartTime = SystemClock.uptimeMillis();
            handler.postDelayed(runnable, 0);

            finishjob.setEnabled(false);*/
           /* long maxTimeInMilliseconds = 30000;// in your case

            startTimer(maxTimeInMilliseconds, 1000);*/

        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST);
        }
    }

    private void startcountdown() {

        focus.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener(){
            @Override
            public void onChronometerTick(Chronometer cArg) {
                Long sec2=TrackerService.getsecond();

                //Long sec=TrackerService.getcountdown(cArg);
                Long  hours = sec2 / 3600;
                Long minutes = (sec2 % 3600) / 60;
                sec2 = sec2 % 60;
                cArg.setText(hours+":"+minutes+":"+sec2);
            }
        });
        focus.setBase(SystemClock.elapsedRealtime());
        focus.start();
    }

    private void getpermissions() {

        permissionManager=new PermissionManager() {

            @Override
            public List<String> setPermission() {
                // If You Don't want to check permission automatically and check your own custom permission
                // Use super.setPermission(); or Don't override this method if not in use
                List<String> customPermission=new ArrayList<>();
                customPermission.add(Manifest.permission.INTERNET);
                customPermission.add(Manifest.permission.ACCESS_FINE_LOCATION);
                return customPermission;
            }
        };

        //To initiate checking permission
        permissionManager.checkAndRequestPermissions(this);
    }

    private void startTrackerService() {
        Intent intent = new Intent(this, TrackerService.class);
//        intent.putExtra("email", email);
//        intent.putExtra("password", password);
        startService(intent);
        //finish();
        /*startService(new Intent(this, TrackerService.class));
        finish();*/
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        permissionManager.checkResult(requestCode,permissions,grantResults);

        ArrayList<String> granted=permissionManager.getStatus().get(0).granted;
        ArrayList<String> denied=permissionManager.getStatus().get(0).denied;

        for(String item:granted)
            Log.e("granted",item);

        for(String item:denied)
            Log.e("granted",item);

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        startTrackerService();

                       /* Double ar[] = new Double[2];
                        ar=TrackerService.getLocation();
                        Log.e("sdd", String.valueOf(ar));*/
                        //Request location updates:
                        //locationManager.requestLocationUpdates(provider, 400, 1, this);
                    }

                } else {
                    finish();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //mDrawerLayout.openDrawer(GravityCompat.START);
                finish();
                return true;

           /* case R.id.action_settings:
                Toast.makeText(MainActivity.this, "Settng", Toast.LENGTH_SHORT).show();
                return true;*/



        }
        return super.onOptionsItemSelected(item);
    }
}
