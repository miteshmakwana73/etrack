package com.example.jenya1.trackme;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText editTextEmail;
    private EditText editTextPassword;
    private EditText editTextName;

    TextView title,reg1,reg2;

    //private Button buttonSignup;
    private ProgressDialog progressDialog;
    private ProgressBar progressBar;


    private FirebaseAuth auth;

    RequestQueue requestQueue;

    String HttpUrl = "http://etrack.rayvatapps.com/api/api.php?request=register";

    LinearLayout login,reg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance();

        //initializing views
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        editTextName = (EditText) findViewById(R.id.editTextName);

        reg=(LinearLayout)findViewById(R.id.lnreg);
        login=(LinearLayout)findViewById(R.id.lnlogin);

        title=(TextView)findViewById(R.id.tvtitle);
        reg1=(TextView)findViewById(R.id.btn_signup);
        reg2=(TextView)findViewById(R.id.btn_signup2);
        //buttonSignup = (Button) findViewById(R.id.buttonSignup);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        progressDialog = new ProgressDialog(this);

        //attaching listener to button
        //buttonSignup.setOnClickListener(this);

        reg.setOnClickListener(this);
        login.setOnClickListener(this);

        Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/HKNova-Medium.ttf");
        editTextEmail.setTypeface(tf);
        editTextPassword.setTypeface(tf);
        editTextName.setTypeface(tf);
        title.setTypeface(tf);
        reg1.setTypeface(tf);
        reg2.setTypeface(tf);



        requestQueue = Volley.newRequestQueue(SignupActivity.this);

        /*btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressBar.setVisibility(View.VISIBLE);
                //create user
                auth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(SignupActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                Toast.makeText(SignupActivity.this, "createUserWithEmail:onComplete:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.GONE);
                                // If sign in fails, display a message to the user. If sign in succeeds
                                // the auth state listener will be notified and logic to handle the
                                // signed in user can be handled in the listener.
                                if (!task.isSuccessful()) {
                                    Toast.makeText(SignupActivity.this, "Authentication failed." + task.getException(),
                                            Toast.LENGTH_SHORT).show();
                                } else {
                                    //startActivity(new Intent(SignupActivity.this, MainActivity.class));
                                    Toast.makeText(SignupActivity.this, "Success", Toast.LENGTH_SHORT).show();
                                    finish();
                                }
                            }
                        });

            }
        });*/
    }

    private void registerUser(){

        //getting email and password from edit texts
        final String email = editTextEmail.getText().toString().trim();
        final String password  = editTextPassword.getText().toString().trim();
        final String name  = editTextName.getText().toString().trim();

        //checking if email and passwords are empty
        if(TextUtils.isEmpty(email) ){
            editTextEmail.setError("Please enter email");
            //Toast.makeText(this,"Please enter email",Toast.LENGTH_LONG).show();
            return;
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches())
        {
            editTextEmail.setError("Enter proper email");
            return;

        }
        if(TextUtils.isEmpty(name)){
            editTextName.setError("Please enter name");
            //Toast.makeText(this,"Please enter password",Toast.LENGTH_LONG).show();
            return;
        }

        if(TextUtils.isEmpty(password)){
            editTextPassword.setError("Please enter password");
            //Toast.makeText(this,"Please enter password",Toast.LENGTH_LONG).show();
            return;
        }

        if (password.length() < 6) {
            editTextPassword.setError("Password too short, enter minimum 6 characters!");
            //Toast.makeText(getApplicationContext(), "Password too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show();
            return;
        }

        //if the email and password are not empty
        //displaying a progress dialog

        progressDialog.setMessage("Registering Please Wait...");
        progressDialog.show();

        /*//creating a new user
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //Log.e("fdf",task.getResult().toString());
                        //checking if success
                        if(task.isSuccessful()){
                            //display some message here
                            Toast.makeText(SignupActivity.this,"Successfully registered",Toast.LENGTH_LONG).show();
                        }else{
                            //display some message here
                            Toast.makeText(SignupActivity.this,"Registration Error "+task.getException().getMessage(),Toast.LENGTH_LONG).show();
                        }
                        progressDialog.dismiss();
                    }
                });
*/

        // Creating string request with post method.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
                        progressDialog.dismiss();
                        try {
                            JSONObject jobj = new JSONObject(ServerResponse);


                            String status = jobj.getString("status");

                            String msg = jobj.getString("message");

                            if (status.equals("1")) {
                                //move to next page
                                Toast.makeText(SignupActivity.this, msg, Toast.LENGTH_SHORT).show();

                            } else {
                                Toast.makeText(SignupActivity.this, msg, Toast.LENGTH_SHORT).show();

                            }

                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
                            Log.e("success",msg);
                        }catch (Exception e) {
                                e.printStackTrace();
                            }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        progressDialog.dismiss();

                        if( volleyError instanceof NoConnectionError) {
                            Toast.makeText(SignupActivity.this, "No connection available", Toast.LENGTH_SHORT).show();
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            Toast.makeText(SignupActivity.this, "Oops. Timeout error!", Toast.LENGTH_LONG).show();
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            Toast.makeText(SignupActivity.this, "Server error!", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(SignupActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }

                       /* String json = null;
                        // Showing error message if something goes wrong.
                        Log.e("Fail",volleyError.toString());

                        if (volleyError.networkResponse == null) {
                            json = volleyError.getMessage();
                        } else {
                            json = volleyError.getMessage() + ", status "
                                    + volleyError.networkResponse.statusCode
                                    + " - " + volleyError.networkResponse.toString();
                        }
                        Log.e("dfdff", json, volleyError);*/
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                params.put("name", name);
                params.put("email", email);
                params.put("password", password);

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(SignupActivity.this);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);

    }


    public String trimMessage(String json, String key){
        String trimmedString = null;

        try{
            JSONObject obj = new JSONObject(json);
            trimmedString = obj.getString(key);
        } catch(JSONException e){
            e.printStackTrace();
            return null;
        }

        return trimmedString;
    }


    //Somewhere that has access to a context
    public void displayMessage(String toastString){
        Toast.makeText(SignupActivity.this, toastString, Toast.LENGTH_LONG).show();
        Log.e("ss",toastString);
    }

    @Override
    protected void onResume() {
        super.onResume();
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.lnreg:
                registerUser();
                //Toast.makeText(this, "...", Toast.LENGTH_SHORT).show();
                break;

            case R.id.lnlogin:
                Intent i = new Intent(SignupActivity.this, LoginActivity.class);
                //i.putExtra("username", uname);
                startActivity(i);
                finish();
                break;
        }
    }
}
