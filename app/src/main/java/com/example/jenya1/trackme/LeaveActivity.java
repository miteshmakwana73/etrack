package com.example.jenya1.trackme;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class LeaveActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private EditText fromDateEtxt;
    private EditText toDateEtxt,reason,days;
    LinearLayout submitliave;

    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;

    private SimpleDateFormat dateFormatter;
    Spinner spin;
    String leavetype,email,from,to,reasons,day;

    private ProgressDialog progressDialog;

    String main_url="http://etrack.rayvatapps.com/api/api.php?request=leaverequest&email=";
    String URL_LEAVE;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.back);
        ab.setTitle("Expenses");
        ab.setDisplayHomeAsUpEnabled(true);

        init();

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

        setDateTimeField();

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.leave_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spin.setAdapter(adapter);
        /*ArrayAdapter<String> adapter2 =
                new ArrayAdapter<String>(this, R.layout.my_spinner_style, R.array.leave_array)
                {

                    public View getView(int position, View convertView, ViewGroup parent) {
                        View v = super.getView(position, convertView, parent);

                        ((TextView) v).setTextSize(16);
                        ((TextView) v).setTextColor(
                                getResources().getColorStateList(R.color.white)
                        );

                        return v;
                    }

                    public View getDropDownView(int position, View convertView, ViewGroup parent) {
                        View v = super.getDropDownView(position, convertView, parent);
                        v.setBackgroundResource(R.drawable.background_button_round);

                        ((TextView) v).setTextColor(
                                getResources().getColorStateList(R.color.white)
                        );

                        //((TextView) v).setTypeface(fontStyle);
                        ((TextView) v).setGravity(Gravity.CENTER);

                        return v;
                    }
                };

        spin.setAdapter(adapter);*/



    }

    private void init() {
        fromDateEtxt = (EditText) findViewById(R.id.edfromdate);
        fromDateEtxt.setInputType(InputType.TYPE_NULL);
        fromDateEtxt.requestFocus();

        toDateEtxt = (EditText) findViewById(R.id.edtodate);
        toDateEtxt.setInputType(InputType.TYPE_NULL);

        spin = (Spinner) findViewById(R.id.spleavetype);
        spin.setOnItemSelectedListener(this);

        submitliave=(LinearLayout)findViewById(R.id.lnmoney);
        reason = (EditText) findViewById(R.id.edreason);
        days = (EditText) findViewById(R.id.edleavedays);

        progressDialog = new ProgressDialog(this);

    }

    private void setDateTimeField() {
        fromDateEtxt.setOnClickListener(this);
        toDateEtxt.setOnClickListener(this);
        submitliave.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                fromDateEtxt.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        toDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                toDateEtxt.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }
    private void checkvalidation() {
         from=fromDateEtxt.getText().toString().trim();
         to=toDateEtxt.getText().toString().trim();
         reasons=reason.getText().toString().trim();
         day=days.getText().toString().trim();

        if (TextUtils.isEmpty(from)) {
            //fromDateEtxt.setError("Select Date!");
                    Toast.makeText(getApplicationContext(), "Select From Date!", Toast.LENGTH_SHORT).show();
            return;
        }

        else if (TextUtils.isEmpty(to)) {
//            toDateEtxt.setError("Select Date!");
                    Toast.makeText(getApplicationContext(), "Select To Date!", Toast.LENGTH_SHORT).show();
            return;
        }
        else if (spin.getSelectedItemPosition() == 0) {
            Toast.makeText(LeaveActivity.this, "Please Select Leave Type", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(reasons)) {
            reason.setError("Write reason!");
//            Toast.makeText(getApplicationContext(), "Select Date!", Toast.LENGTH_SHORT).show();
            return;
        }
        else if (TextUtils.isEmpty(day)) {
            days.setError("Enter Days");
//            Toast.makeText(getApplicationContext(), "Select Date!", Toast.LENGTH_SHORT).show();
            return;
        }
        else 
        {
            SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
             email = sharedPreferences.getString("username","");

            URL_LEAVE=main_url+email+"&from_date="+from+"&to_date="+to+"&type="+leavetype+"&reason="+reasons+"&noofdays="+day;
            submitleave();
        }
    }

    private void submitleave() {
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();

        /*//creating a new user
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //Log.e("fdf",task.getResult().toString());
                        //checking if success
                        if(task.isSuccessful()){
                            //display some message here
                            Toast.makeText(SignupActivity.this,"Successfully registered",Toast.LENGTH_LONG).show();
                        }else{
                            //display some message here
                            Toast.makeText(SignupActivity.this,"Registration Error "+task.getException().getMessage(),Toast.LENGTH_LONG).show();
                        }
                        progressDialog.dismiss();
                    }
                });
*/

        // Creating string request with post method.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_LEAVE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
                        progressDialog.dismiss();
                        try {
                            JSONObject jobj = new JSONObject(ServerResponse);


                            String status = jobj.getString("status");

                            String msg = jobj.getString("message");

                            if (status.equals("1")) {
                                //move to next page
                                Toast.makeText(LeaveActivity.this, msg, Toast.LENGTH_SHORT).show();

                            } else {
                                Toast.makeText(LeaveActivity.this, msg, Toast.LENGTH_SHORT).show();

                            }

                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        progressDialog.dismiss();

                        if( volleyError instanceof NoConnectionError) {
                            Toast.makeText(LeaveActivity.this, "No connection available", Toast.LENGTH_SHORT).show();
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            Toast.makeText(LeaveActivity.this, "Oops. Timeout error!", Toast.LENGTH_LONG).show();
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            Toast.makeText(LeaveActivity.this, "Server error!", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(LeaveActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }

                       /* String json = null;
                        // Showing error message if something goes wrong.
                        Log.e("Fail",volleyError.toString());

                        if (volleyError.networkResponse == null) {
                            json = volleyError.getMessage();
                        } else {
                            json = volleyError.getMessage() + ", status "
                                    + volleyError.networkResponse.statusCode
                                    + " - " + volleyError.networkResponse.toString();
                        }
                        Log.e("dfdff", json, volleyError);*/
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                params.put("email", email);
                params.put("from_date", from);
                params.put("to_date", to);
                params.put("type", leavetype);
                params.put("reason", reasons);
                params.put("noofdays", day);

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(LeaveActivity.this);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //mDrawerLayout.openDrawer(GravityCompat.START);
                finish();
                return true;

           /* case R.id.action_settings:
                Toast.makeText(MainActivity.this, "Settng", Toast.LENGTH_SHORT).show();
                return true;*/



        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.edfromdate:
                fromDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                fromDatePickerDialog.show();
                break;

            case R.id.edtodate:
                toDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                toDatePickerDialog.show();
                break;

            case R.id.lnmoney:
                checkvalidation();
                break;
        }
    }




    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Toast.makeText(this, "Selected "+spin.getSelectedItem(), Toast.LENGTH_SHORT).show();
        leavetype= String.valueOf(spin.getSelectedItem());
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        Toast.makeText(this, "Select Leave type", Toast.LENGTH_SHORT).show();
    }
}
