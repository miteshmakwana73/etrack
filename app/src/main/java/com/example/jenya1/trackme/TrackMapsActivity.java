package com.example.jenya1.trackme;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.jenya1.trackme.model.InfoWindowData;
import com.example.jenya1.trackme.service.TrackerService;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class TrackMapsActivity extends AppCompatActivity implements OnMapReadyCallback,GoogleMap.InfoWindowAdapter {

    private static final String TAG = TrackMapsActivity.class.getSimpleName();
    private HashMap<String, Marker> mMarkers = new HashMap<>();
    private GoogleMap mMap;

    ImageView mapdesign,logout,menu;
    String email,password;

    LocationManager manager;
    boolean statusOfGPS ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        manager=  (LocationManager) getSystemService(Context.LOCATION_SERVICE );


        mapdesign=(ImageView)findViewById(R.id.imgmapdesign);

        logout=(ImageView)findViewById(R.id.imglogout);

        mapdesign.setImageResource(R.mipmap.ic_mapdesign);

        mapdesign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mapdesign.getDrawable().getConstantState() ==
                        getResources().getDrawable(R.mipmap.ic_mapdesign).getConstantState()){
                    mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                    mapdesign.setImageResource(R.mipmap.ic_mapdesignselect);
                    Toast.makeText(TrackMapsActivity.this, "HYBRID", Toast.LENGTH_SHORT).show();
                } else{
                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    mapdesign.setImageResource(R.mipmap.ic_mapdesign);
                    Toast.makeText(TrackMapsActivity.this, "NORMAL", Toast.LENGTH_SHORT).show();                }
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logoutUser();
            }
        });

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(this, 1 * 60 * 1000); // every 2 minutes
                Log.e("Dealy","Dealy");
                checkLocationonoroff();
                if(statusOfGPS==false)
                {
                    Intent i=new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(i);
                    Toast.makeText(TrackMapsActivity.this, "Please Enable Location...", Toast.LENGTH_SHORT).show();                }
            }
        }, 1 * 60 * 1000);


       /* Bundle extras = getIntent().getExtras();
        if(extras == null) {
            email="0000";
        } else {
            email= extras.getString("email");
            password= extras.getString("password");
            Log.e("email", String.valueOf(email));
            Log.e("password ",password);
        }*/
    }

    private void logoutUser() {
        Intent intent = new Intent(TrackMapsActivity.this,LoginActivity.class);
        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
        startActivity(intent);
        finish();
    }

    private void checkLocationonoroff() {

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE );
        statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

       /* if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) )
            Toast.makeText(getApplicationContext(), "GPS is disable!", Toast.LENGTH_LONG).show();
        else
            Toast.makeText(getApplicationContext(), "GPS is Enable!", Toast.LENGTH_LONG).show();

        Log.e("location", String.valueOf(manager));*/

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        mMap.setMaxZoomPreference(16);
        loginToFirebase();
    }

    private void loginToFirebase() {

        email="miteshmakwana@gmail.com";
        password="mitesh@123";

        /*SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
        email = sharedPreferences.getString("username","");
        password = sharedPreferences.getString("password","");*/

        FirebaseAuth.getInstance().signInWithEmailAndPassword(
                email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    subscribeToUpdates();
                    Log.d(TAG, "firebase auth success");
                } else {
                    Log.d(TAG, "firebase auth failed");
                }
            }
        });
    }

    private void subscribeToUpdates() {
        // Functionality coming next step
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference(getString(R.string.firebase_path));
        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                setMarker(dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
                setMarker(dataSnapshot);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.d(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    private void setMarker(DataSnapshot dataSnapshot) {
        // Functionality coming next step
        // When a location update is received, put or update
        // its value in mMarkers, which contains all the markers
        // for locations received, so that we can build the
        // boundaries required to show them all on the map at once
        String key = dataSnapshot.getKey();
        HashMap<String, Object> value = (HashMap<String, Object>) dataSnapshot.getValue();
        double lat = Double.parseDouble(value.get("latitude").toString());
        double lng = Double.parseDouble(value.get("longitude").toString());

        String add=getAddress(this,lat,lng);
        add = add.replace("null","");

       /* add = add.replace("]","");
        add = add.replace(".","");
        add = add.replace("#","','");
        add = add.replace("$","')");
        add = add.replace("@","");
        add = add.replace("gmail","");
        add = add.replace("com","");*/
        Log.e("details",add);
        LatLng location = new LatLng(lat, lng);

        if (!mMarkers.containsKey(key)) {

            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(location)
                    .title(key)
                    .snippet(add)
                    .icon(BitmapDescriptorFactory.defaultMarker( BitmapDescriptorFactory.HUE_RED));

            InfoWindowData info = new InfoWindowData();
            info.setImage(String.valueOf(R.drawable.userl));
            info.setHotel("Hotel : excellent hotels available");
            info.setFood("Food : all types of restaurants available");
            info.setTransport("Reach the site by bus, car and train.");

            CustomInfoWindowGoogleMap customInfoWindow = new CustomInfoWindowGoogleMap(this);
            mMap.setInfoWindowAdapter(customInfoWindow);

            Marker m = mMap.addMarker(markerOptions);
            m.setTag(info);
            m.showInfoWindow();

            /*ClusterLayer cl = new ClusterLayer();
            cl.addMarker(m);*/


            mMarkers.put(key,m);
            //mMarkers.put(key, mMap.addMarker(new MarkerOptions().title(key+" "+add).position(location)));
        } else {
            mMarkers.get(key).setPosition(location);
        }
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Marker marker : mMarkers.values()) {
            builder.include(marker.getPosition());
        }
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 300));
    }

    public String getAddress(Context context, double lat, double lng) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);

            String add = obj.getAddressLine(0);
            add = add + "\n" + obj.getCountryName();
            add = add + "\n" + obj.getCountryCode();
            add = add + "\n" + obj.getAdminArea();
            add = add + "\n" + obj.getPostalCode();
            add = add + "\n" + obj.getSubAdminArea();
            add = add + "\n" + obj.getLocality();
            add = add + "\n" + obj.getSubThoroughfare();

            return add;
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            return null;
        }
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

}
