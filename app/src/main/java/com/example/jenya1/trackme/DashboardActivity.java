package com.example.jenya1.trackme;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.jenya1.trackme.adapter.DashboardAdapter;
import com.example.jenya1.trackme.model.Dashboard;
import com.example.jenya1.trackme.service.TrackerService;
import com.karan.churi.PermissionManager.PermissionManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DashboardActivity extends AppCompatActivity {

    //Button startjob,finishjob,startlaunch,overlaunch;

    Typeface tf;
    String email,password;

    Date currentTime;
    private RecyclerView recyclerView;
    private DashboardAdapter adapter;
    private List<Dashboard> albumList;

    PermissionManager permissionManager;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_new);

        getpermissions();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        albumList = new ArrayList<>();
        adapter = new DashboardAdapter(this, albumList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        prepareAlbums();

        try {
            Glide.with(this).load(R.drawable.cover).into((ImageView) findViewById(R.id.backdrop));
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*startjob=(Button)findViewById(R.id.btn_startjob);
        finishjob=(Button)findViewById(R.id.btn_finishjob);*/
        /*startlaunch=(Button)findViewById(R.id.btn_startlaunch);
        overlaunch=(Button)findViewById(R.id.btn_overlaunch);*/


        tf = Typeface.createFromAsset(this.getAssets(), "fonts/HKNova-Medium.ttf");
        /*startjob.setTypeface(tf);
        finishjob.setTypeface(tf);*/
       /* startlaunch.setTypeface(tf);
        overlaunch.setTypeface(tf);*/


       /* Bundle extras = getIntent().getExtras();
        if(extras == null) {
            email="0000";
        } else {
            email= extras.getString("email");
            password= extras.getString("password");
            Log.e("email", String.valueOf(email));
            Log.e("password ",password);
        }*/

        /*startjob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashboardActivity.this, TrackerActivity.class);
//                intent.putExtra("email", email);
//                intent.putExtra("password", password);
                startActivity(intent);
                //finish();
            }
        });

        finishjob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopService(new Intent(getApplicationContext(), TrackerService.class));

                *//*Intent intent = new Intent(DashboardActivity.this, TrackerService.class);
                //unbindService(serviceConnection);
                 stopService(intent);*//*
                *//*TrackerService.destroy_service();
                TrackerService.unbindService();*//*
            }
        });*/


        /*startlaunch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentTime = Calendar.getInstance().getTime();

                Toast.makeText(DashboardActivity.this, "Start Launch \n"+currentTime, Toast.LENGTH_SHORT).show();
            }
        });

        overlaunch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentTime = Calendar.getInstance().getTime();

                Toast.makeText(DashboardActivity.this, "Start Launch \n"+currentTime, Toast.LENGTH_SHORT).show();

            }
        });*/
    }

    private void getpermissions() {

        permissionManager=new PermissionManager() {

            @Override
            public List<String> setPermission() {
                // If You Don't want to check permission automatically and check your own custom permission
                // Use super.setPermission(); or Don't override this method if not in use
                List<String> customPermission=new ArrayList<>();
                customPermission.add(android.Manifest.permission.INTERNET);
                customPermission.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
                return customPermission;
            }
        };

        //To initiate checking permission
        permissionManager.checkAndRequestPermissions(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        permissionManager.checkResult(requestCode,permissions,grantResults);

        ArrayList<String> granted=permissionManager.getStatus().get(0).granted;
        ArrayList<String> denied=permissionManager.getStatus().get(0).denied;

        for(String item:granted)
            Log.e("granted",item);

        for(String item:denied)
            Log.e("granted",item);

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        //startTrackerService();
                        Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();

                       /* Double ar[] = new Double[2];
                        ar=TrackerService.getLocation();
                        Log.e("sdd", String.valueOf(ar));*/
                        //Request location updates:
                        //locationManager.requestLocationUpdates(provider, 400, 1, this);
                    }

                } else {
                    finish();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }
        }

    }

    private void prepareAlbums() {
        int[] covers = new int[]{
                R.drawable.work,
                R.drawable.break_time,
                R.drawable.money,
                R.drawable.leave,
                R.drawable.report};

        Dashboard a = new Dashboard("Work",  covers[0]);
        albumList.add(a);

        a = new Dashboard("Break",  covers[1]);
        albumList.add(a);

        a = new Dashboard("Expenses",  covers[2]);
        albumList.add(a);

        a = new Dashboard("Leave",  covers[3]);
        albumList.add(a);

        a = new Dashboard("Report",  covers[4]);
        albumList.add(a);

        adapter.notifyDataSetChanged();
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    private void logoutUser() {
        Intent intent = new Intent(DashboardActivity.this,LoginActivity.class);
        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action bar actions click
        switch (item.getItemId()) {
            /*case R.id.rate_app:
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
                }
                return true;*/
            case R.id.action_logout:
                logoutUser();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
