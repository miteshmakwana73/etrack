package com.example.jenya1.trackme.service;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.jenya1.trackme.LaunchActivity;
import com.example.jenya1.trackme.LoginActivity;
import com.example.jenya1.trackme.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.Manifest;
import android.location.LocationManager;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

public class TrackerService extends Service {

    private static final String TAG = TrackerService.class.getSimpleName();
    String email,password;
    static String hhh="00",mmm="00",sss="00";

    public static int tracker_service_status=0;

    private Timer counter = new Timer();
    static private long seconds ,launchseconds,minutes;
    private TimerTask clockTask = null;
    private static TimerTask clockTasklaunch = null;

    static double lat;
    static double lonng;

    String URL_LOCATION = "http://etrack.rayvatapps.com/api/api.php?request=livestatus";

    LocationRequest request;

    LocationManager manager;
    boolean statusOfGPS ;

    @Override
    public IBinder onBind(Intent intent) {return null;}

    @Override
    public void onCreate() {
        super.onCreate();
        buildNotification();
        manager=  (LocationManager) getSystemService(Context.LOCATION_SERVICE );

        clockTask = new TimerTask() {
            public void run() {
                seconds++;
            }
        };

        startCounter();

        final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    handler.postDelayed(this, 1 * 60 * 1000); // every 2 minutes
                                    Log.e("Dealy","Dealy");
                                    checkLocationonoroff();
                                    if(statusOfGPS==false)
                                    {
                                        Toast.makeText(TrackerService.this, "Please Enable Location...", Toast.LENGTH_SHORT).show();
                                    }
//                                     your code here
                                    if(tracker_service_status==1) {
                                        sendtoserver();
                                    }

                                }
                            }, 1 * 60 * 1000);

    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);

        /*Bundle extras = intent.getExtras();
        if(extras == null) {
            email="0000";
        } else {
            email= extras.getString("email");
            password= extras.getString("password");
            Log.e("email service", String.valueOf(email));
            Log.e("password service ",password);
        }*/
        loginToFirebase();
    }

    private void buildNotification() {
        String stop = "stop";
        registerReceiver(stopReceiver, new IntentFilter(stop));
        PendingIntent broadcastIntent = PendingIntent.getBroadcast(
                this, 0, new Intent(stop), PendingIntent.FLAG_UPDATE_CURRENT);
        // Create the persistent notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.notification_text2))
                .setOngoing(true)
                .setContentIntent(broadcastIntent)
                .setSmallIcon(R.drawable.ic_tracker);
        startForeground(1, builder.build());

    }

    protected BroadcastReceiver stopReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "received stop broadcast");
            // Stop the service when the notification is tapped
            //unregisterReceiver(stopReceiver);
           // stopSelf();
        }
    };

    private void loginToFirebase() {
        // Functionality coming next step
        // Authenticate with Firebase, and request location updates
        /*String email = getString(R.string.firebase_email);
        String password = getString(R.string.firebase_password);*/

        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
        email = sharedPreferences.getString("username","");
        password = sharedPreferences.getString("password","");
        String email_login="miteshmakwana@gmail.com";
        String password_login="mitesh@123";

        FirebaseAuth.getInstance().signInWithEmailAndPassword(
                email_login, password_login).addOnCompleteListener(new OnCompleteListener<AuthResult>(){
            @Override
            public void onComplete(Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "firebase auth success");
                    requestLocationUpdates();
                } else {
                    Log.d(TAG, "firebase auth failed");
                }
            }
        });
    }

    private void requestLocationUpdates() {
        tracker_service_status=1;
        // Functionality coming next step
       /* String[] str = email.split(",(?=(?:[^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)", -1);  // defining 3 columns with null or blank field //values acceptance
        string = string.replace("to", "xyz");*/

        String sid = email;
        sid = sid.replace("[","");
        sid = sid.replace("]","");
        sid = sid.replace(".","");
        sid = sid.replace("#","','");
        sid = sid.replace("$","')");
        sid = sid.replace("@","");
        sid = sid.replace("gmail","");
        sid = sid.replace("com","");

         request = new LocationRequest();
        request.setInterval(10000); //150000
        request.setFastestInterval(5000);
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        FusedLocationProviderClient client = LocationServices.getFusedLocationProviderClient(this);
        final String path = getString(R.string.firebase_path) + "/" + sid;            //getString(R.string.transport_id);
        int permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permission == PackageManager.PERMISSION_GRANTED) {
            // Request location updates and when an update is
            // received, store the location in Firebase
            client.requestLocationUpdates(request, new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference(path);
                    Location location = locationResult.getLastLocation();
                    if (location != null) {
                        Log.d(TAG, "location update " + location);
                        //sendtoserver();

                       /* request.setInterval(1000000);

                        final Handler handler = new Handler();
                        *//* your code here *//*
                        new Runnable() {
                            @Override
                            public void run() {
                                handler.postDelayed(this, 4 * 60 * 1000); // every 2 minutes
                                if(tracker_service_status==1) {
                                    sendtoserver();
                                }
                            }
                        }.run();*/

                        /*final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    handler.postDelayed(this, 1 * 60 * 1000); // every 2 minutes
                                    Log.e("Dealy","Dealy");
                                    *//* your code here *//*
                                    if(tracker_service_status==1) {
                                        sendtoserver();
                                    }

                                }
                            }, 1 * 60 * 1000);*/

                        lat= location.getLatitude();
                        lonng=location.getLongitude();
                        //Toast.makeText(TrackerService.this, "Location update"+location, Toast.LENGTH_SHORT).show();
                        ref.setValue(location);
                    }
                }
            }, null);
        }
    }

    private void checkLocationonoroff() {
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE );
        statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        /*final LocationManager manager = (LocationManager)getApplicationContext().getSystemService(Context.LOCATION_SERVICE );

        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) )
            Toast.makeText(getApplicationContext(), "GPS is disable!", Toast.LENGTH_LONG).show();
        else
            Toast.makeText(getApplicationContext(), "GPS is Enable!", Toast.LENGTH_LONG).show();*/
    }

    private void sendtoserver() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_LOCATION,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
                        try {
                            JSONObject jobj = new JSONObject(ServerResponse);


                            String status = jobj.getString("status");

                            String msg = jobj.getString("message");

                            if (status.equals("1")) {
                                //move to next page
                                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();

                            } else {
                                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                            }

                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.

                        if( volleyError instanceof NoConnectionError) {
                            Toast.makeText(getApplicationContext(), "No connection available", Toast.LENGTH_SHORT).show();
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            Toast.makeText(getApplicationContext(), "Oops. Timeout error!", Toast.LENGTH_LONG).show();
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            Toast.makeText(getApplicationContext(), "Server error!", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                email = sharedPreferences.getString("username","");
                password = sharedPreferences.getString("password","");

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String date=sdf.format(new Date());
                Log.e("Date",date);

                SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
                String time=sdf2.format(new Date());
                Log.e("Time",time);

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                //params.put("name", name);
                params.put("email", email);
                params.put("date", date);
                params.put("time", time);
                params.put("latitude", String.valueOf(lat));
                params.put("longitude", String.valueOf(lonng));

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    @Override
    public void onDestroy() {
        //this.BroadcastReceiver();
            // TODO Auto-generated method stub
        seconds = 0L;
        clockTask.cancel();
        Log.e(" stop", String.valueOf(seconds));

            try{
                if(stopReceiver!=null)
                    unregisterReceiver(stopReceiver);
                    stopSelf();

            }catch(Exception e)
            {
                    Log.e("Service Destroy Error",e.getMessage());
            }
        super.onDestroy();
    }

    private void startCounter() {
        //set up a timer to increment seconds once per second (1000 milliseconds):
        counter.scheduleAtFixedRate(clockTask, 0, 1000L);
    }

    public static Long getsecond() { return seconds;}

    public static Double[] getLocation() {
        Double ar[] = new Double[2];
        ar[0]= lat;
        ar[1] =  lonng;
        return ar;
    }

    public static Long getlaunchsecond() {
        clockTasklaunch = new TimerTask() {
            public void run() {
                launchseconds++;
            }
        };

        return launchseconds;
    }
}