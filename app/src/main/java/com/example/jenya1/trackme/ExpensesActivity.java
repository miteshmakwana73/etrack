package com.example.jenya1.trackme;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.jenya1.trackme.adapter.ReportAdapter;
import com.example.jenya1.trackme.model.Report;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ExpensesActivity extends AppCompatActivity {

    LinearLayout money;
    EditText amount,reason;

    String stramount,strreason;
    String email,time;

    private ProgressDialog progressDialog;


    String main_url="http://etrack.rayvatapps.com/api/api.php?request=expenses&email=";
    String URL_EXPENSES;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expenses);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.back);
        ab.setTitle("Expenses");
        ab.setDisplayHomeAsUpEnabled(true);

        money=(LinearLayout)findViewById(R.id.lnmoney);
        amount=(EditText)findViewById(R.id.edamount);
        reason=(EditText)findViewById(R.id.edreason);
        progressDialog = new ProgressDialog(this);


        money.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stramount = amount.getText().toString().trim();
                strreason = reason.getText().toString().trim();

                if (TextUtils.isEmpty(stramount)) {
                    amount.setError("Enter amount!");
//                    Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(strreason)) {
                    reason.setError("Enter reason!");
//                    Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                    return;
                }

                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                email = sharedPreferences.getString("username","");

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                time=sdf.format(new Date());
                Log.e("Time",time);

                URL_EXPENSES=main_url+email+"&amount="+stramount+"&reason="+strreason+"&time="+time;

                //Log.e("Url ",URL_EXPENSES);

                submitdata();
            }
        });
    }

    private void submitdata() {

        final String stramount = amount.getText().toString().trim();
        final String strreason  = reason.getText().toString().trim();

        progressDialog.setMessage("Please Wait...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_EXPENSES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
                        progressDialog.dismiss();
                        try {
                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status = jobj.getString("status");

                            String msg = jobj.getString("message");

                            if (status.equals("1")) {
                                //move to next page
                                Toast.makeText(ExpensesActivity.this, msg, Toast.LENGTH_SHORT).show();

                            } else {
                                Toast.makeText(ExpensesActivity.this, msg, Toast.LENGTH_SHORT).show();

                            }

                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
                            //Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        progressDialog.dismiss();

                        if( volleyError instanceof NoConnectionError) {
                            Toast.makeText(ExpensesActivity.this, "No connection available", Toast.LENGTH_SHORT).show();
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            Toast.makeText(ExpensesActivity.this, "Oops. Timeout error!", Toast.LENGTH_LONG).show();
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            Toast.makeText(ExpensesActivity.this, "Server error!", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(ExpensesActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }

                       /* String json = null;
                        // Showing error message if something goes wrong.
                        Log.e("Fail",volleyError.toString());

                        if (volleyError.networkResponse == null) {
                            json = volleyError.getMessage();
                        } else {
                            json = volleyError.getMessage() + ", status "
                                    + volleyError.networkResponse.statusCode
                                    + " - " + volleyError.networkResponse.toString();
                        }
                        Log.e("dfdff", json, volleyError);*/
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                params.put("email", email);
                params.put("amount", stramount);
                params.put("reason", strreason);
                params.put("time", time);

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(ExpensesActivity.this);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //mDrawerLayout.openDrawer(GravityCompat.START);
                finish();
                return true;

           /* case R.id.action_settings:
                Toast.makeText(MainActivity.this, "Settng", Toast.LENGTH_SHORT).show();
                return true;*/



        }
        return super.onOptionsItemSelected(item);
    }

}
