package com.example.jenya1.trackme.model;

/**
 * Created by Lincoln on 18/05/16.
 */
public class Report {

    private String Date;
    private String Time;

    public Report(String date, String time) {
        Date = date;
        Time = time;
    }

    public Report() {

    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }
}