package com.example.jenya1.trackme.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.ClipboardManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jenya1.trackme.R;
import com.example.jenya1.trackme.model.Report;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ravi Tamada on 18/05/16.
 */
public class ReportAdapter extends RecyclerView.Adapter<ReportAdapter.MyViewHolder> {

    private Context mContext;
    private List<Report> albumList;
    private List lstfavquoteid;

    String quote_id;
    ArrayList<Integer> arryfavquoteid ;

    Typeface tf;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView date, hours;

        public MyViewHolder(View view) {
            super(view);

            date = (TextView) view.findViewById(R.id.txtdate);
            hours = (TextView) view.findViewById(R.id.txthours);

        }
    }

    public ReportAdapter(Context mContext, List<Report> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }
    public ReportAdapter(Context mContext, List<Report> albumList, ArrayList<String> arryfavquoteid) {
        this.mContext = mContext;
        this.albumList = albumList;
        this.lstfavquoteid=arryfavquoteid;
    }
 
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_report, parent, false);
 
        return new MyViewHolder(itemView);
    }
 
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        arryfavquoteid = new ArrayList<Integer>();

        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
        holder.date.setTypeface(tf);
        holder.hours.setTypeface(tf);
       // holder.count.setTypeface(tf);

        final Report album = albumList.get(position);
        holder.date.setText(album.getDate().trim());
        holder.hours.setText(album.getTime().trim());

    }
 
    @Override
    public int getItemCount() {
        return albumList.size();
    }
}