package com.example.jenya1.trackme.service;

import android.Manifest;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.example.jenya1.trackme.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Timer;
import java.util.TimerTask;

public class LaunchService extends Service {

    private static final String TAG = LaunchService.class.getSimpleName();
    String email,password;
    static String hhh="00",mmm="00",sss="00";

    public static int launch_service_status=0;

    private Timer counter = new Timer();
    static private long seconds ,launchseconds,minutes;
    private TimerTask clockTask = null;
    private static TimerTask clockTasklaunch = null;

    static double lat;
    static double lonng;
    LaunchService mService=this;
    @Override
    public IBinder onBind(Intent intent) {return null;}

    @Override
    public void onCreate() {
        super.onCreate();
        //buildNotification();

        clockTask = new TimerTask() {
            public void run() {
                seconds++;
            }
        };
        startCounter();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);

        /*Bundle extras = intent.getExtras();
        if(extras == null) {
            email="0000";
        } else {
            email= extras.getString("email");
            password= extras.getString("password");
            Log.e("email service", String.valueOf(email));
            Log.e("password service ",password);
        }*/
        loginToFirebase();
    }

    private void buildNotification() {
        String stop = "stop";
        registerReceiver(stopReceiver, new IntentFilter(stop));
        PendingIntent broadcastIntent = PendingIntent.getBroadcast(
                this, 0, new Intent(stop), PendingIntent.FLAG_UPDATE_CURRENT);
        // Create the persistent notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.notification_text3))
                .setOngoing(true)
                .setContentIntent(broadcastIntent)
                .setSmallIcon(R.drawable.ic_tracker);
        startForeground(1, builder.build());
    }

    protected BroadcastReceiver stopReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "received stop broadcast");
            // Stop the service when the notification is tapped
            //unregisterReceiver(stopReceiver);
           // stopSelf();
        }
    };

    private void loginToFirebase() {
        // Functionality coming next step
        // Authenticate with Firebase, and request location updates
        /*String email = getString(R.string.firebase_email);
        String password = getString(R.string.firebase_password);*/

        /*SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
        email = sharedPreferences.getString("username","");
        password = sharedPreferences.getString("password","");*/
        email="miteshmakwana@gmail.com";
        password="mitesh@123";

        FirebaseAuth.getInstance().signInWithEmailAndPassword(
                email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>(){
            @Override
            public void onComplete(Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "firebase auth success");
                    requestLocationUpdates();
                } else {
                    Log.d(TAG, "firebase auth failed");
                }
            }
        });
    }

    private void requestLocationUpdates() {
        launch_service_status=1;
        // Functionality coming next step
       /* String[] str = email.split(",(?=(?:[^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)", -1);  // defining 3 columns with null or blank field //values acceptance
        string = string.replace("to", "xyz");*/

        String sid = email;
        sid = sid.replace("[","");
        sid = sid.replace("]","");
        sid = sid.replace(".","");
        sid = sid.replace("#","','");
        sid = sid.replace("$","')");
        sid = sid.replace("@","");
        sid = sid.replace("gmail","");
        sid = sid.replace("com","");

        LocationRequest request = new LocationRequest();
        request.setInterval(10000);
        request.setFastestInterval(5000);
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        FusedLocationProviderClient client = LocationServices.getFusedLocationProviderClient(this);
        final String path = getString(R.string.firebase_path) + "/" + sid;            //getString(R.string.transport_id);
        int permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permission == PackageManager.PERMISSION_GRANTED) {
            // Request location updates and when an update is
            // received, store the location in Firebase
            client.requestLocationUpdates(request, new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference(path);
                    Location location = locationResult.getLastLocation();
                    if (location != null) {
                        Log.d(TAG, "location update " + location);
                        lat= location.getLatitude();
                        lonng=location.getLongitude();
                        //Toast.makeText(TrackerService.this, "Location update"+location, Toast.LENGTH_SHORT).show();
                        ref.setValue(location);
                    }
                }
            }, null);
        }
    }

    @Override
    public void onDestroy() {
        //this.BroadcastReceiver();
            // TODO Auto-generated method stub
        seconds = 0L;
        clockTask.cancel();
        Log.e(" stop", String.valueOf(seconds));

        try{
                if(stopReceiver!=null)
                    //unregisterReceiver(stopReceiver);
                    stopSelf();
            }catch(Exception e)
            {
                    Log.e("Service Destroy Error",e.getMessage());
            }
        super.onDestroy();
    }

    private void startCounter() {
        //set up a timer to increment seconds once per second (1000 milliseconds):
        counter.scheduleAtFixedRate(clockTask, 0, 1000L);
    }

    public static Long getsecond() { return seconds; }

    public static Double[] getLocation() {
        Double ar[] = new Double[2];
        ar[0]= lat;
        ar[1] =  lonng;
        return ar;    }

    public static Long getlaunchsecond() {
        clockTasklaunch = new TimerTask() {
            public void run() {
                launchseconds++;
            }
        };

        return launchseconds;
    }
}