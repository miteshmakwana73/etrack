package com.example.jenya1.trackme.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.jenya1.trackme.ExpensesActivity;
import com.example.jenya1.trackme.LaunchActivity;
import com.example.jenya1.trackme.LeaveActivity;
import com.example.jenya1.trackme.R;
import com.example.jenya1.trackme.ReportActivity;
import com.example.jenya1.trackme.TrackerActivity;
import com.example.jenya1.trackme.model.Dashboard;

import java.util.List;
 
/**
 * Created by Ravi Tamada on 18/05/16.
 */
public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.MyViewHolder> {
 
    private Context mContext;
    private List<Dashboard> albumList;
    Intent intent;
 
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView thumbnail;
        CardView layout;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            layout = (CardView) view.findViewById(R.id.card_view);
        }
    }
 
 
    public DashboardAdapter(Context mContext, List<Dashboard> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }
 
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_dashboard, parent, false);
 
        return new MyViewHolder(itemView);
    }
 
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Dashboard album = albumList.get(position);
        holder.title.setText(album.getTitle());

        Typeface tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
        holder.title.setTypeface(tf);

        // loading album cover using Glide library
        Glide.with(mContext).load(album.getThumbnail()).into(holder.thumbnail);

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickhandle(position);
            }
        });

        holder.thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickhandle(position);
            }
        });
 
    }

    private void clickhandle(int position) {
        if(position==0)
        {
            intent = new Intent(mContext,TrackerActivity.class);
            mContext.startActivity(intent);
        }
        else if(position==1)
        {
            intent = new Intent(mContext,LaunchActivity.class);
            mContext.startActivity(intent);
        }
        else if(position==2)
        {
            intent = new Intent(mContext,ExpensesActivity.class);
            mContext.startActivity(intent);
        }
        else if(position==3)
        {
            intent = new Intent(mContext,LeaveActivity.class);
            mContext.startActivity(intent);
        }
        else if(position==4)
        {
            intent = new Intent(mContext,ReportActivity.class);
            mContext.startActivity(intent);
        }
        else
        {
            Toast.makeText(mContext, "try...", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Showing popup menu when tapping on 3 dots
     */

 
    @Override
    public int getItemCount() {
        return albumList.size();
    }
}