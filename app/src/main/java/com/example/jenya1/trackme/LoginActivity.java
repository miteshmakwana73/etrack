package com.example.jenya1.trackme;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private EditText inputEmail, inputPassword;
    TextView title,reg1,reg2;
    private FirebaseAuth auth;
    private ProgressBar progressBar;
    //private Button btnSignup, btnLogin, btnReset;

    String email;
    String password;

    String URL_LOGIN = "http://etrack.rayvatapps.com/api/api.php?request=login";


    LinearLayout login,reg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance();

        CheckLogin();

        /*if (auth.getCurrentUser() != null) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }*/

        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/

        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        title=(TextView)findViewById(R.id.tvtitle);
        reg1=(TextView)findViewById(R.id.btn_signup);
        reg2=(TextView)findViewById(R.id.btn_signup2);
       /* btnSignup = (Button) findViewById(R.id.btn_signup);
        btnLogin = (Button) findViewById(R.id.btn_login);
        btnReset = (Button) findViewById(R.id.btn_reset_password);*/
        login=(LinearLayout)findViewById(R.id.lnlogin);
        reg=(LinearLayout)findViewById(R.id.lnreg);

        Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/HKNova-Medium.ttf");
        inputEmail.setTypeface(tf);
        inputPassword.setTypeface(tf);
        title.setTypeface(tf);
        reg1.setTypeface(tf);
        reg2.setTypeface(tf);


        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance();

        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, SignupActivity.class));
                finish();
            }
        });

       /* btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, ResetPasswordActivity.class));
            }
        });*/

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 email = inputEmail.getText().toString().trim();
                 password = inputPassword.getText().toString().trim();

                if (TextUtils.isEmpty(email)) {
                    inputEmail.setError("Enter email address!");
//                    Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    inputPassword.setError("Enter password!");
//                    Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);

                // Creating string request with post method.
                StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_LOGIN,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String ServerResponse) {

                                // Hiding the progress dialog after all task complete.
                                progressBar.setVisibility(View.GONE);
                                try {
                                    JSONObject jobj = new JSONObject(ServerResponse);


                                    String status = jobj.getString("status");

                                    String msg = jobj.getString("message");

                                    if (status.equals("1")) {
                                        //move to next page
                                        //Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);

                                        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.putString("username",email);
                                        editor.putString("password",password);
                                        editor.commit();

                                        if(email.equalsIgnoreCase("m@m.m")) {
                                            Intent i = new Intent(LoginActivity.this, TrackMapsActivity.class);
                                            //i.putExtra("username", uname);
                                            startActivity(i);
                                            finish();
                                        }
                                        else
                                        {
                                            Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                                            startActivity(intent);
                                            finish();
                                        }

//                                        intent.putExtra("email", email);
//                                        intent.putExtra("password", password);
                                        //startActivity(intent);
                                        //finish();
                                        Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();

                                    } else if (status.equals("2")) {

                                        showLocationDialog(msg);
                                        //move to next page

                                       /* new AlertDialog.Builder(LoginActivity.this).setMessage(msg)
                                                .setTitle("Account")
                                                .setNeutralButton(android.R.string.ok,
                                                        new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int whichButton){
                                                                finish();
                                                            }
                                                        })
                                                .show();*/
                                        /*AlertDialog.Builder builder;
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                            builder = new AlertDialog.Builder(LoginActivity.this, android.R.style.Theme_Material_Light_Dialog);
                                        } else {
                                            builder = new AlertDialog.Builder(LoginActivity.this);
                                        }
                                        builder.setTitle("Account")
                                                .setMessage(msg)
                                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        // continue with delete
                                                    }
                                                })
                                                .setIcon(android.R.drawable.ic_dialog_alert)
                                                .show();
                                       // Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();*/

                                    }else {
                                        Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();

                                    }

                                    //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
                                    Log.e("success",msg);
                                }catch (Exception e) {
                                    e.printStackTrace();
                                }

                                // Showing response message coming from server.

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError volleyError) {

                                // Hiding the progress dialog after all task complete.
                                progressBar.setVisibility(View.GONE);

                                if( volleyError instanceof NoConnectionError) {
                                    Toast.makeText(LoginActivity.this, "No connection available", Toast.LENGTH_SHORT).show();
                                }
                                else if (volleyError.getClass().equals(TimeoutError.class)) {
                                    // Show timeout error message
                                    Toast.makeText(LoginActivity.this, "Oops. Timeout error!", Toast.LENGTH_LONG).show();
                                }
                                else if (volleyError instanceof ServerError) {
                                    // Show timeout error message
                                    Toast.makeText(LoginActivity.this, "Server error!", Toast.LENGTH_LONG).show();
                                }
                                else
                                {
                                    Toast.makeText(LoginActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                                }
                            }
                        }) {
                    @Override
                    protected Map<String, String> getParams() {

                        // Creating Map String Params.
                        Map<String, String> params = new HashMap<String, String>();

                        // Adding All values to Params.
                        //params.put("name", name);
                        params.put("email", email);
                        params.put("password", password);

                        return params;
                    }

                };

                // Creating RequestQueue.
                RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);

                // Adding the StringRequest object into requestQueue.
                requestQueue.add(stringRequest);

                /*//authenticate user
                auth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                // If sign in fails, display a message to the user. If sign in succeeds
                                // the auth state listener will be notified and logic to handle the
                                // signed in user can be handled in the listener.
                                progressBar.setVisibility(View.GONE);
                                if (!task.isSuccessful()) {
                                    // there was an error
                                    if (password.length() < 6) {
                                        inputPassword.setError(getString(R.string.minimum_password));
                                    } else {
                                        Toast.makeText(LoginActivity.this, getString(R.string.auth_failed), Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    Toast.makeText(LoginActivity.this, "Success...", Toast.LENGTH_SHORT).show();
                                    //Log.e("eail",email);
                                    if (email.equals("admin@gmail.com")) {
                                        Intent admin_intent = new Intent(LoginActivity.this, TrackMapsActivity.class);

                                        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.putString("username",email);
                                        editor.putString("password",password);
                                        editor.commit();

//                                        admin_intent.putExtra("email", email);
//                                        admin_intent.putExtra("password", password);
                                        startActivity(admin_intent);
                                        finish();
                                        //Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                                    } else {
                                        Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);

                                        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.putString("username",email);
                                        editor.putString("password",password);
                                        editor.commit();

//                                        intent.putExtra("email", email);
//                                        intent.putExtra("password", password);
                                        startActivity(intent);
                                        finish();
                                       // Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());

                                    }
                                   *//* Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                    startActivity(intent);*//*
//                                    finish();
                                }
                            }
                        });*/
            }
        });
    }


    private void showLocationDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(LoginActivity.this, android.R.style.Theme_Material_Light_Dialog);
        } else {
            builder = new AlertDialog.Builder(LoginActivity.this);
        }
        builder.setTitle(getString(R.string.dialog_title));
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setMessage(msg);

        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // positive button logic
                    }
                });

        /*String negativeText = getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // negative button logic
                    }
                });*/

        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();
    }

    private void CheckLogin() {
        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
        String uname = sharedPreferences.getString("username","");
        if (uname.equalsIgnoreCase("")) {

        }else if(uname.equalsIgnoreCase("m@m.m")) {
            Intent i = new Intent(LoginActivity.this, TrackMapsActivity.class);
            //i.putExtra("username", uname);
            startActivity(i);
            finish();
        }
        else
        {
            Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
            startActivity(intent);
            finish();
        }
    }

}
